# Day 0

## GKE Cluster and Nodepools

## Steps followed
- Enable Compute Engine API
![alt](./screenshots/Screenshot%202023-09-11%20011840.png)
- Enable Kubernetes Engine API
![alt](./screenshots/Screenshot%202023-09-11%20011941.png)
- Created a new VPC "default"
![alt](./screenshots/Screenshot%202023-09-11%20014013.png)
- Created a bucket "anudeep-tf-bkt-1"
![alt](./screenshots/Screenshot%202023-09-11%20012221.png)
- Created a service account with "Editor" role
![alt](./screenshots/Screenshot%202023-09-11%20012521.png)
- Added "Service Accont Token Creator" role to owner account
![alt](./screenshots/Screenshot%202023-09-11%20013626.png)
- Run `terraform init` to intialize terraform
![alt](./screenshots/Screenshot%202023-09-11%20013502.png)
- Run `terraform plan` to verify the configurations
![alt](./screenshots/Screenshot%202023-09-11%20013534.png)
- Run `terraform apply` to apply the final configurations
    - For the prompts of ip ranges just press Enter  
    
![alt](./screenshots/Screenshot%202023-09-11%20013803.png)
![alt](./screenshots/Screenshot%202023-09-11%20015330.png)
![alt](./screenshots/Screenshot%202023-09-11%20021615.png)
- Connected to gke cluster using the gcloud command for saving kubeconfig
- Once the nodes are up, created a yaml file for basic nginx-deployment
- Verified the deployment using `kubectl get pods -l app=nginx`
![alt](./screenshots/Screenshot%202023-09-11%20024115.png)
- Find the screenshots in the screenshots directory [screenshots](./screenshots/)
- Resources used
    - https://developer.hashicorp.com/terraform/tutorials/kubernetes/gke
    - https://cloud.google.com/blog/topics/developers-practitioners/using-google-cloud-service-account-impersonation-your-terraform-code
    - https://www.youtube.com/watch?v=X_IK0GBbBTw&ab_channel=AntonPutra
    - https://github.com/hashicorp/learn-terraform-provision-gke-cluster/blob/main/versions.tf
    - https://antonputra.com/google/create-gke-cluster-using-terraform/
    - https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/
    - https://ziimm.medium.com/how-to-setup-a-terraform-remote-backend-in-google-cloud-6bb3f1829d5c
    - https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/using_gke_with_terraform
